<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'talidb');

/** Имя пользователя MySQL */
define('DB_USER', 'taliuser');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', 'aXjW44b4pM6yXu9N');

/** Имя сервера MySQL */
define('DB_HOST', 'mysqldb');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8mb4');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_?+P1:`nv!@*Wp<,-gZ.<x)lJ)357Xy6%-Uw)TmUY2@Z5WB_aEk)mQ<G3&4!rWzL');
define('SECURE_AUTH_KEY',  '0AhWU#mJK(SMUXh8c7077-K^Q_(FtsUl8#76QuP]L1?*[|a8cBq[+@<DHuuH0122');
define('LOGGED_IN_KEY',    '#h>rOnLe$]<[j1I_]3/hFS-?6eA-0Nn[=h)I&<!NSC89v53`,?/1=U*N#[W<.54:');
define('NONCE_KEY',        '2,||W7V?frBA2|^C_B&i97pyx8UtWFl($vy,9K6f0#TSY>`Cw!6al(,0l2f#!Pu7');
define('AUTH_SALT',        '!q!+rG(Loh%+4f a!=>6esh:oVwCpGWv<<#ESV5fR6FcA1_9EB,CpJKf-G9psQ9]');
define('SECURE_AUTH_SALT', '1FtgyXJk6Pd7D)V^5h3CeES-&g]=jmhn$NK+dH~m|Ovxg&zh@D: cSY,^j(ISi_b');
define('LOGGED_IN_SALT',   '/$){>~]-1K DE IE^(xs48N(lYo3,~w!iXl #hl>QZd]?9}4MO>-wFH(^XGS?6vc');
define('NONCE_SALT',       '#~*_bQ[yX`tCMa7BADLluW7{bWu0*m}?Bh)ie1Fwb$dcE0=BN]OCv#4tJ6Zu^8pl');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
