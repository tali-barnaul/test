# Барнаульские Тали
Тестовое задани

## Подготовка
Чтобы выполнить тестовое задание вам понадобится [Docker](https://www.docker.com/). Установите его на свой компьютер, после этого поднимите рабочее окружение выполнив в терминале:

```bash
$ make docker-start
$ make mysql-restore
```

Теперь откройте в браузере [http://localhost:8000](http://localhost:8000), админка тут: [http://localhost:8000/wp-admin](http://localhost:8000/wp-admin). 

Доступы:
```
Логин: admin
Пароль: secret
```

## Задание
Вам нужно зайти в админку и создать новый пост со своим именем. После этого выполнить в терминале:

```bash
$ make mysql-dump
$ make docker-stop
$ git add . && git commit -m "Ваше имя" && git push
```

После этого создать pull request из ветки develop в master. 