#!/usr/bin/env bash
include .env

# MySQL
MYSQL_DUMPS_DIR=data/db/dumps

help:
	@echo ""
	@echo "usage: make COMMAND"
	@echo ""
	@echo "Commands:"
	@echo "  clean               Clean directories for reset"
	@echo "  docker-start        Create and start containers"
	@echo "  docker-stop         Stop and clear all services"
	@echo "  gen-certs           Generate SSL certificates"
	@echo "  logs                Follow log output"
	@echo "  mysql-dump          Create backup of all databases"
	@echo "  mysql-restore       Restore backup of all databases"
	@echo "  mysql-init          Database initialisation"

clean:
	@rm -Rf data/db/mysql/*
	@rm -Rf etc/ssl/*

docker-start:
	sudo docker-compose up -d

docker-stop:
	@sudo docker-compose down -v
	@make clean

gen-certs:
	@sudo docker run --rm -v $(shell pwd)/etc/ssl:/certificates -e "SERVER=$(NGINX_HOST)" jacoelho/generate-certificate

logs:
	@sudo docker-compose logs -f

mysql-init:
	@sudo docker exec -i $(shell docker-compose ps -q mysqldb) mysqladmin --user="$(MYSQL_ROOT_USER)" --password="$(MYSQL_ROOT_PASSWORD)" --host="mysqldb" create $(MYSQL_DATABASE)
	@sudo docker exec -i $(shell docker-compose ps -q mysqldb) mysql --user="$(MYSQL_ROOT_USER)" --password="$(MYSQL_ROOT_PASSWORD)" --host="mysqldb" $(MYSQL_DATABASE)  < $(MYSQL_DUMPS_DIR)/db.sql

mysql-dump:
	@mkdir -p $(MYSQL_DUMPS_DIR)
	@sudo docker exec $(shell docker-compose ps -q mysqldb) mysqldump -u"$(MYSQL_ROOT_USER)" -p"$(MYSQL_ROOT_PASSWORD)" $(MYSQL_DATABASE) > $(MYSQL_DUMPS_DIR)/db.sql
	@make reset-owner

mysql-restore:
	@sudo docker exec -i $(shell docker-compose ps -q mysqldb) mysqladmin --user="$(MYSQL_ROOT_USER)" --password="$(MYSQL_ROOT_PASSWORD)" --host="mysqldb" --force drop $(MYSQL_DATABASE)
	@make mysql-init

reset-owner:
	@$(shell chown -Rf $(SUDO_USER):$(shell id -g -n $(SUDO_USER)) $(MYSQL_DUMPS_DIR) "$(shell pwd)/etc/ssl" "$(shell pwd)/web/app" 2> /dev/null)
